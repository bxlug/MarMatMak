<?php
// **************************************************************************
// Copyright (C) 2007 BxLUG - http://www.bxlug.be
// Please submit comments and suggestions to devel [�] lists.bxlug.be
// **************************************************************************
// This file is part of � MarMatMak �
// a software to easily create marketing material
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details:
//                 http://www.gnu.org/copyleft/gpl.html
// ***************************************************************************

//
// Dependencies
//
require_once('./lib/templates.php');

function get_preview_url() {
  global $templates;

  header('Content-Type: text/html; charset=ISO-8859-15');

  if (isset($_GET['id'])) {
    if (isset($templates->list[$_GET['id']])) {
      $template = $templates->list[$_GET['id']];
      // Preview image URL of first template
      print("templates/{$template->id}/{$template->preview_img}");
    } else {
      print("Template [{$_GET['id']}] does not exists.");
    }
  }
  else {
    print('No template id specified.');
  }
}

function get_template_data() {
  global $templates;

  header('Content-Type: text/xml; charset=UTF-8');
  print("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n");
  print("<marmakmat xmlns=\"http://www.bxlug.be/ns/marmakmat\">\n");

  if (isset($_GET['id'])) {
    if (isset($templates->list[$_GET['id']])) {
      $template = $templates->list[$_GET['id']];
      print("<preview src=\"templates/{$template->id}/{$template->preview_img}\"/>\n");
      print("<description><![CDATA[{$template->description}]]></description>\n");
      print($template->texts->asXML());
    }
    else
    {
      print("<error>Template [{$_GET['id']}] does not exists.</error>");
    }
  }
  else
  {
    print('<error>No template id specified.</error>');
  }

  print("\n</marmakmat>");
}

if (isset($_GET['fct']))
{
  switch ($_GET['fct'])
  {
    case 'purl':
      get_preview_url();
      break;
    case 'tdata':
      get_template_data();
      break;
    default:
      print("Unknown function [{$_GET['fct']}]");
  }
}
else
{
  print("No function specified");
}
?>