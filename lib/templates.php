<?php
// **************************************************************************
// Copyright (C) 2007 BxLUG - http://www.bxlug.be
// Please submit comments and suggestions to devel [�] lists.bxlug.be
// **************************************************************************
// This file is part of � MarMatMak �
// a software to easily create marketing material
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details:
//                 http://www.gnu.org/copyleft/gpl.html
// ***************************************************************************

require_once('./lib/simplexml/IsterXmlSimpleXMLImpl.php');

class TemplateList {
  var $list = array();

  function TemplateList() {
    $this->load_templates();
  }

  // Scan disk to discover all available templates
  function load_templates() {
    $this->list = array();
    $dh = opendir('./templates/');
    while (false !== ($filename = readdir($dh))) {
      if (file_exists("./templates/{$filename}/config.xml")) {
        $tpl = new Template;
        $tpl->load($filename);
        $this->list[$filename] = $tpl;
      }
    }

    return $this->list;
  }
}

class Template {
  var $id                  = 'default';
  var $template_name       = 'Default Template';
  var $description         = '';
  var $font_family         = 'Helvetica';
  var $font_family_options = 'B';
  var $texts               = array();
  var $background_img      = '';
  var $preview_img         = 'preview.png';
  var $row_count           = 1;
  var $column_count        = 1;
  var $document_width      = 210;
  var $document_height     = 297;
  var $left_margin         = 25;
  var $top_margin          = 25;
  var $draw_border         = true;

  function load($id) {
    $this->id = $id;
    if (file_exists("./templates/{$id}/config.xml")) {
      $impl = new IsterXmlSimpleXMLImpl;
      // Remove all loggers to avoid errors on XML parsing
      $impl->deleteLogger("IsterLoggerStd");
      $doc  = $impl->load_file("./templates/{$id}/config.xml");
      // Name of this template
      $this->template_name  = $doc->marmatmak->name->CDATA();
      // Template description
      if (isset($doc->marmatmak->description))
        $this->description    = $doc->marmatmak->description->CDATA();
      // Get document layout
      if (isset($doc->marmatmak->document)) {
        $attr = $doc->marmatmak->document->attributes();
        if (isset($attr['rows'])    && is_numeric($attr['rows']))    $this->row_count       = $attr['rows'];
        if (isset($attr['columns']) && is_numeric($attr['columns'])) $this->column_count    = $attr['columns'];
        if (isset($attr['width'])   && is_numeric($attr['width']))   $this->document_width  = $attr['width'];
        if (isset($attr['height'])  && is_numeric($attr['height']))  $this->document_height = $attr['height'];
        if (isset($attr['lmargin']) && is_numeric($attr['lmargin'])) $this->left_margin     = $attr['lmargin'];
        if (isset($attr['tmargin']) && is_numeric($attr['tmargin'])) $this->top_margin      = $attr['tmargin'];
      }
      // Get background image
      if (isset($doc->marmatmak->background)) {
        $attr = $doc->marmatmak->background->attributes();
        $this->background_img = $attr['src'];
      }
      // Get preview image (if specified)
      if (isset($doc->marmatmak->preview)) {
        $attr = $doc->marmatmak->preview->attributes();
        $this->preview_img = $attr['src'];
      }
      // Get list of customizable texts that can be display on the document
      $this->texts = $doc->marmatmak->texts;
      // Check all attributes are set, otherwise, use default
      for ($i=0; $i<count($this->texts->text); $i++) {
        $attr = $this->texts->text[$i]->attributes();
        if (!isset($attr['x']))         $this->texts->text[$i]->setAttribute('x', 1);
        if (!isset($attr['y']))         $this->texts->text[$i]->setAttribute('y', 1);
        if (!isset($attr['font_size'])) $this->texts->text[$i]->setAttribute('font_size', 12);
        if (!isset($attr['label']))     $this->texts->text[$i]->setAttribute('label', "Attribut {$i}");
      }
    }
  }
}

$templates = new TemplateList();
?>