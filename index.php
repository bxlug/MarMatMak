<?php
// **************************************************************************
// Copyright (C) 2007 BxLUG - http://www.bxlug.be
// Please submit comments and suggestions to devel [à] lists.bxlug.be
// **************************************************************************
// This file is part of « MarMatMak »
// a software to easily create marketing material
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details:
//                 http://www.gnu.org/copyleft/gpl.html
// ***************************************************************************

// Overrides server configuration and force charset
header('Content-Type: text/html; charset=utf-8');

//
// Dependencies
//
require_once('lib/templates.php');

// Form options of list of available templates 
$template_option_html = '';
foreach ($templates->list as $template) {
  $template_option_html .= "<option value=\"{$template->id}\">{$template->template_name}</option>";
}

print <<<EOF
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Générateur de matériel promotionnel</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" type="text/css" href="http://www.bxlug.be/css/faty.css" media="screen" title="Faty" />
  <script type="text/javascript" src="js/main.js"></script>
</head>

<body onload="change_template();">
<div id="top">
  <h1 id="bxlug"><span>BxLUG</span> groupe des utilisateurs de GNU/Linux de Bruxelles</h1>
</div>

<div id="main-content">

  <h1>Générateur de matériel promotionnel</h1>

  <p>Cet outil vous permet de générer facilement le matériel promotionnel des activités du BxLUG.
  Choisissez le type de document dont vous avez besoin dans la liste proposée dans « Modèle »,
  remplissez les champs puis appuyez le bouton « Créer le document ».  Un fichier de type « PDF »
  sera créé et téléchargé sur votre ordinateur afin d&rsquo;être imprimé par vos soins.</p>

  <form name="form1" method="post" action="makeDocument.php">
    <table id="tpl_table" width="90%" border="0" align="center" cellpadding="0" cellspacing="5">
      <tr id="preview" valign="center">
        <td width="340" rowspan="20" align="right"><img src="" id="preview_img"></td>
        <td align="right"><label for="tpl">Mod&egrave;le</label></td>
        <td>
        <select name="tpl" id="tpl" onchange='change_template();'>
          {$template_option_html}
        </select>
        </td>
      </tr>
      <tr id="tpl_desc" valign="center" style="display: none;">
        <td align="right"><label for="tpl">Description</label></td>
        <td>Pas de description disponible.</td>
      </tr>
      <tr valign="center">
        <td colspan="2" align="center"><input type="submit" name="Submit" value="Créer le document"></td>
      </tr>
    </table>
  </form>

  <p>Si vous désirez ajouter un modèle ou obtenir plus d&rsquo;informormation sur cet outil,
  consultez la <a href="doc.php" title="Documentation de MarMatMak">documentation</a></p>
</div>

<div id="footer">
<p>Copyright 2007 BxLUG - Groupe des Utilisateurs de GNU/Linux de Bruxelles
</div>
</body>
</html>
EOF;
?>
